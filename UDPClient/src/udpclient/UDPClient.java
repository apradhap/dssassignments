package udpclient;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Random;

public class UDPClient {

	private DatagramSocket socket;	
	private InetAddress serverIP;
	
	private byte[] clientIP;
	private short port;
	private long requestTime;
	private byte[] randomID = new byte[2];
		
	private byte[] requestPacket;
	//private byte[] responsePacket;
	
	public UDPClient() throws SocketException, UnknownHostException
	{
		socket = new DatagramSocket();
		clientIP = InetAddress.getLocalHost().getAddress();
		port = (short) socket.getLocalPort();
		requestTime = System.nanoTime();
		
		Random randomByte = new Random();
		randomByte.nextBytes(randomID);
		
		serverIP = InetAddress.getByName("34.209.99.236");
		
		System.out.println(clientIP);
		System.out.println(port);
		System.out.println(requestTime);
		System.out.println(randomID);
		System.out.println(serverIP);
		
	}
	

	
	public void createRequestPacket(int id)
	{
		ByteBuffer uniqueID = ByteBuffer.allocate(16);
		uniqueID.order(ByteOrder.LITTLE_ENDIAN);
		uniqueID.put(clientIP);
		uniqueID.putShort(port);
		uniqueID.put(randomID);
		uniqueID.putLong(requestTime);
		
		ByteBuffer studentID = ByteBuffer.allocate(4);
		studentID.order(ByteOrder.LITTLE_ENDIAN);
		studentID.putInt(id);
		
		ByteBuffer requestBuffer = ByteBuffer.allocate(uniqueID.capacity() + studentID.capacity());
		requestBuffer.put(uniqueID);
		requestBuffer.put(studentID);
		
		requestPacket = requestBuffer.array();
		return;
		
	}
	
	public String getStudentCode() throws IOException
	{
		
		DatagramPacket packet = new DatagramPacket(requestPacket, requestPacket.length, serverIP, 43101);
		
		System.out.println("Socket created");
		System.out.println(socket.isConnected());
		
		socket.send(packet);
		
		System.out.println("Request sent");
		
		//byte[] responsePacket;
		packet = new DatagramPacket(requestPacket, requestPacket.length);
        socket.receive(packet);
        
        socket.setSoTimeout(100);
        socket.close();
        
        System.out.println("Response received");
        
        String studentCode = new String(packet.getData(), 0, packet.getLength());
		
		return studentCode;
		
	}
	
	public void closeSocket()
	{
		socket.close();
	}
	
	public static void main(String[] args) throws IOException, UnknownHostException {
		// TODO Auto-generated method stub
		UDPClient client = new UDPClient();
		client.createRequestPacket(1381632);
		String code = client.getStudentCode();
		
		System.out.println(code);
	}

}
